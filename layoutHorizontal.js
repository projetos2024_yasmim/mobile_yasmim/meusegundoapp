import { StatusBar } from 'expo-status-bar';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import Layout from './layoutTelaEstrutura';

export default function LayoutHorizontal() {
  return (
    <View style={styles.container}>
        
        <ScrollView horizontal={true}>
        <View style={styles.box1}></View>
        <View style={styles.box2}></View>
        <View style={styles.box3}></View>
        <View style={styles.box1}></View>
        <View style={styles.box2}></View>
        <View style={styles.box3}></View>
        <View style={styles.box1}></View>
        <View style={styles.box2}></View>
        <View style={styles.box3}></View>
        <View style={styles.box1}></View>
        <View style={styles.box2}></View>
        <View style={styles.box3}></View>
        <View style={styles.box1}></View>
        <View style={styles.box2}></View>
        <View style={styles.box3}></View>
        <View style={styles.box1}></View>
        <View style={styles.box2}></View>
        <View style={styles.box3}></View>
        </ScrollView>


        
    </View>

   
    

    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection:"row",
  },

  box1:{
    width:50,
    height:50,
    backgroundColor: "#D65DB1",
    borderRadius:50,
    marginLeft: 5,
   
  }, 

  box2:{
    width:50,
    height:50,
    backgroundColor:"#FF6F91",
    borderRadius:50,
    marginLeft: 5,
    

  }, 

  box3:{
    width:50,
    height:50,
    backgroundColor:"#FF9671",
    borderRadius:50,
    marginLeft:5,
  }, 



  

  

});
