import { StatusBar } from 'expo-status-bar';
import { ScrollView, StyleSheet, Text, View } from 'react-native';


export default function LayoutGrade() {
  return (
    <View style={styles.container}>
        
        <View style={styles.linha}>
            <View style={styles.box1}></View>
            <View style={styles.box2}></View>
        </View>
        <View style={styles.linha}>
            <View style={styles.box3}></View>
            <View style={styles.box4}></View>
        </View>
    </View>
    

    

    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent:'center',
    alignItems: 'center',
  },

    linha:{
        flexDirection:'row',

    },

    box1:{
        width:50,
        height:50,
        backgroundColor:"#D6EEEB",
    },

    box2:{
        width:50,
        height:50,
        backgroundColor:"#C7E4D9",
    },

    box3:{
        width:50,
        height:50,
        backgroundColor:"#C1E9FC",
    },

    box4:{
        width:50,
        height:50,
        backgroundColor:"#96DAEE",
    },

});