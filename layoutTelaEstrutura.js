import { StatusBar } from 'expo-status-bar';
import { ScrollView, StyleSheet, Text, View } from 'react-native';

export default function Layout() {
  return (
    <View style={styles.container}>
     <View style={styles.header}>
      <Text style={styles.text}>MENU</Text>
    
     </View>
     <View style={styles.content}>
      <ScrollView>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
        <Text style={styles.text}>CONTEUDO</Text>
      </ScrollView>
      
     </View>
     <View style={styles.footer}>
      <Text style={styles.text}>RODAPE</Text>
     </View>
      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection:"column",
    justifyContent: 'space-between',
  },

  header:{
    height:50,
    backgroundColor:'pink',
  },

  content:{
    flex:1,
    backgroundColor:'white',

  },

  footer:{
    height:50,
    backgroundColor:'red',

  },

  text:{
    fontSize:16,
    fontWeight:'bold',
    textAlign:'center',

  },
});
